# Temperature Converter CLI

Converts temperature between Fahrenheit and Celsius.

## Usage

Compile with `go build -o temp` (for windows use `go build -o temp.exe` so you can execute the file).

Then, invoke the binary passing as argument the unit of temperature we want to convert **from**.
For example:

`./temp C` to convert from Celsius to Fahrenheit or `./temp F` to convert from Fahrenheit to Celsius.
